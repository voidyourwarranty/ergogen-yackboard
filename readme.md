# Yackboard ([Y]et [A]nother [C]ustom [K]ey Board)

This is a fork of the [ergogen](https://www.github.com/ergogen/ergogen) project by Dénes Bán which I have adapted in
order to produce the [Yackboard](https://gitlab.com/voidyourwarranty/yackboard).

For an introduction to the ergogen project, I recommend
- [Conference presentation](https://www.youtube.com/watch?v=5tERUZ_BSPM) by Dénes Bán
- [Official Documentation](https://docs.ergogen.xyz/)
- [The REAL ergonomic keyboard endgame](https://www.youtube.com/watch?v=UKfeJrRIcxw) by Ben Vallack
- [Design your own keyboard](https://www.youtube.com/watch?v=M_VuXVErD6E) by Ben Vallack

During the early stages of developing my keyboard, I iterated the design on the (inofficial)
[online server](https://ergogen.cache.works/).

The present fork is based on the commit `v3.1.2` of the `master` branch of `ergogen`. It works with `node` `v12.22.9`
and `npm` `8.5.1` that are standard on Ubuntu 22.04 LTS. More recent commits of the `develop` branch fail to run with
these versions.

Version 1 of the Yackboard was produced from the commit of Dec 25, 2022 without any manual intervention to any footprint
or outline in KiCAD. Before you produce a keybaord, please carefully read the
[discussion](https://gitlab.com/voidyourwarranty/yackboard) for a couple of minor mechanical shortcomings.

The following is the original README of the `ergogen` project.

# Ergogen

Ergogen is a keyboard generator that aims to provide a common configuration format to describe **ergonomic** 2D layouts, and generate automatic plates, cases, and (un-routed) PCBs for them.
The project grew out of (and is an integral part of) the [Absolem keyboard](https://zealot.hu/absolem), and shares its [Discord server](https://discord.gg/nbKcAZB) as well.

For usage and config information, please refer to the [docs](https://docs.ergogen.xyz).


## Contributions

Feature ideas, documentation improvements, examples, tests, or pull requests welcome!
Get in touch [on Discord](https://discord.gg/nbKcAZB), and we can definitely find something you can help with, if you'd like to.
