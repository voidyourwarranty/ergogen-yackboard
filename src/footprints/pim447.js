//
// This is the purely mechanical footprint for mounting a Pimoroni Track Ball PIM 447.
// Note that wiring is done by hand and no connectors are included.
//
module.exports = {
    nets: {
    },
    params: {
        class: 'P', // for Pointer
        side: 'F'
    },
    body: p => `
    (module Pim447 (layer "F.Cu") (tedit 63717012)

      ${p.at /* parametric position */}
      ${'' /* footprint reference */}

      ${'' /* outline */}
      (fp_rect (start -13.5 -7.5) (end -8.5 7.5) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_rect (start -7 -7) (end 7 7) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_rect (start -8.5 -12.5) (end 8.5 12.5) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_circle (center 0 0) (end 3 0) (layer "User.Drawings") (width 0.12) (fill none))

      ${'' /* pins */}
      (pad "" thru_hole circle (at 5.25 9.75) (size 3.3 3.3) (drill 2.7) (layers *.Cu *.Mask))
      (pad "" thru_hole circle (at -5.25 -9.75) (size 3.3 3.3) (drill 2.7) (layers *.Cu *.Mask))
      (pad "" thru_hole circle (at -5.25 9.75) (size 3.3 3.3) (drill 2.7) (layers *.Cu *.Mask))
      (pad "" thru_hole circle (at 5.25 -9.75) (size 3.3 3.3) (drill 2.7) (layers *.Cu *.Mask))
    )`
}
