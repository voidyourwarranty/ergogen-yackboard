//
// This is the footprint of my reset button, 0.2" spaced through holes.
//
module.exports = {
    nets: {
        from: undefined,
        to: undefined
    },
    params: {
        class: 'B', // for Button
        side: 'F'
    },
    body: p => `
    (module Button2 (layer "F.Cu") (tedit 63717012)

      ${p.at /* parametric position */}
      ${'' /* footprint reference */}
      ${'' /* outline */}
      (fp_rect (start -3.25 -3.25) (end 3.25 3.25) (layer "User.Drawings") (width 0.12) (fill none))

      ${'' /* pins */}
      (pad "1" thru_hole circle (at -2.54 0) (size 1.524 1.524) (drill 0.762) (layers *.Cu *.Mask) ${p.net.from.str})
      (pad "2" thru_hole circle (at 2.54 0) (size 1.524 1.524) (drill 0.762) (layers *.Cu *.Mask) ${p.net.to.str})
    )`
}
