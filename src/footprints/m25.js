//
// This is an M2.5 ISO mounting hole.
// Most PCB manufacturers have a higher precision of this is simply a plated through-hole.
//
module.exports = {
    nets: {
    },
    params: {
        class: 'H', // for Hole
        side: 'F'
    },
    body: p => `
    (module MountingHoleM2.5 (layer "F.Cu") (tedit 63717012)

      ${p.at /* parametric position */}
      ${'' /* footprint reference */}

      ${'' /* outline */}

      ${'' /* pins */}
      (pad "" thru_hole circle (at 0 0) (size 3.3 3.3) (drill 2.7) (layers *.Cu *.Mask))
    )`
}
