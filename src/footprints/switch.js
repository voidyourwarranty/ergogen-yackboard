//
// This is the footprint of my on/off switch, three through holes with 0.1" spacing.
//
module.exports = {
    nets: {
        on: undefined,
        mid: undefined,
        off: undefined
    },
    params: {
        class: 'S', // for Switch
        side: 'F'
    },
    body: p => `
    (module Switch (layer "F.Cu") (tedit 63717012)

      ${p.at /* parametric position */}
      ${'' /* footprint reference */}
      ${'' /* outline */}
      (fp_rect (start -4.3 -2) (end 4.3 2) (layer "User.Drawings") (width 0.12) (fill none))

      ${'' /* pins */}
      (pad "1" thru_hole circle (at -2.54 0) (size 1.524 1.524) (drill 0.762) (layers *.Cu *.Mask) ${p.net.on.str})
      (pad "2" thru_hole circle (at 0 0) (size 1.524 1.524) (drill 0.762) (layers *.Cu *.Mask) ${p.net.mid.str})
      (pad "3" thru_hole circle (at 2.54 0) (size 1.524 1.524) (drill 0.762) (layers *.Cu *.Mask) ${p.net.off.str})
    )`
}
