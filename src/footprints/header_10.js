//
// This is a 0.1" spaced custom pin header which exposes all unused GPIOs of the microcontroller.
//
module.exports = {
    nets: {
        vcc: undefined,
        p00: undefined,
        p01: undefined,
        p02: undefined,
        p03: undefined,
        p04: undefined,
        p05: undefined,
        p20: undefined,
        p21: undefined,
        gnd: undefined
    },
    params: {
        class: 'H',
        side: 'F'
    },
    body: p => `
    (module Header10 (layer "F.Cu") (tedit 63717012)

      ${p.at /* parametric position */}
      ${'' /* footprint reference */}
      (fp_text reference "VCC" (at -2.45 -11.43 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp df32840e-2912-4088-b54c-9a85f64c0265)
      )
      (fp_text user "5" (at -2.45 3.81 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp 1ab71a3c-340b-469a-ada5-4f87f0b7b2fa)
      )
      (fp_text user "3" (at -2.45 -1.27 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp 25bc3602-3fb4-4a04-94e3-21ba22562c24)
      )
      (fp_text user "21" (at -2.45 8.89 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp 3c9169cc-3a77-4ae0-8afc-cbfc472a28c5)
      )
      (fp_text user "GND" (at -2.45 11.43 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp 44035e53-ff94-45ad-801f-55a1ce042a0d)
      )
      (fp_text user "2" (at -2.45 -3.81 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp 90e761f6-1432-4f73-ad28-fa8869b7ec31)
      )
      (fp_text user "1" (at -2.45 -6.35 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp 9f782c92-a5e8-49db-bfda-752b35522ce4)
      )
      (fp_text user "20" (at -2.45 6.35 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp a25b7e01-1754-4cc9-8a14-3d9c461e5af5)
      )
      (fp_text user "4" (at -2.45 1.27 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp d38aa458-d7c4-47af-ba08-2b6be506a3fd)
      )
      (fp_text user "0" (at -2.45 -8.89 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp d7e4abd8-69f5-4706-b12e-898194e5bf56)
      )
      (fp_text user "5" (at -2.45 3.81 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp 088f77ba-fca9-42b3-876e-a6937267f957)
      )
      (fp_text user "4" (at -2.45 1.27 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp 0f324b67-75ef-407f-8dbc-3c1fc5c2abba)
      )
      (fp_text user "GND" (at -2.45 11.43 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp 28e37b45-f843-47c2-85c9-ca19f5430ece)
      )
      (fp_text user "0" (at -2.45 -8.89 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp 29e058a7-50a3-43e5-81c3-bfee53da08be)
      )
      (fp_text user "1" (at -2.45 -6.35 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp 5edcefbe-9766-42c8-9529-28d0ec865573)
      )
      (fp_text user "20" (at -2.45 6.35 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp 795e68e2-c9ba-45cf-9bff-89b8fae05b5a)
      )
      (fp_text user "VCC" (at -2.45 -11.43 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp 994b6220-4755-4d84-91b3-6122ac1c2c5e)
      )
      (fp_text user "3" (at -2.45 -1.27 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp e4d2f565-25a0-48c6-be59-f4bf31ad2558)
      )
      (fp_text user "2" (at -2.45 -3.81 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp f449bd37-cc90-4487-aee6-2a20b8d2843a)
      )
      (fp_text user "21" (at -2.45 8.89 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp fa918b6d-f6cf-4471-be3b-4ff713f55a2e)
      )

      ${'' /* outline */}
      ${'' /* pins */}
      (pad "1" thru_hole circle (at 0 -11.43) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask) ${p.net.vcc.str})
      (pad "2" thru_hole circle (at 0 -8.89) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask)  ${p.net.p00.str})
      (pad "3" thru_hole circle (at 0 -6.35) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask)  ${p.net.p01.str})
      (pad "4" thru_hole circle (at 0 -3.81) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask)  ${p.net.p02.str})
      (pad "5" thru_hole circle (at 0 -1.27) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask)  ${p.net.p03.str})
      (pad "6" thru_hole circle (at 0 1.27) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask)   ${p.net.p04.str})
      (pad "7" thru_hole circle (at 0 3.81) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask)   ${p.net.p05.str})
      (pad "8" thru_hole circle (at 0 6.35) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask)   ${p.net.p20.str})
      (pad "9" thru_hole circle (at 0 8.89) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask)   ${p.net.p21.str})
      (pad "10" thru_hole circle (at 0 11.43) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask) ${p.net.gnd.str})
    )`
}
