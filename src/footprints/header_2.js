//
// This is a 0.1" spaced custom pin header for soldering a battery cable.
//
module.exports = {
    nets: {
        bat: undefined,
        gnd: undefined
    },
    params: {
        class: 'H',
        side: 'F'
    },
    body: p => `
    (module Header2 (layer "F.Cu") (tedit 63717012)

      ${p.at /* parametric position */}
      ${'' /* footprint reference */}
      (fp_text reference "BAT" (at 2.54 -1.27 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp 563c12e4-8f8c-446c-a11f-94f5aa93b994)
      )
      (fp_text user "GND" (at -2.54 1.27 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp 0a3cc030-c9dd-4d74-9d50-715ed2b361a2)
      )
      (fp_text user "BAT" (at -2.54 -1.27 unlocked) (layer "B.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)) (justify mirror))
        (tstamp 854dd5d4-5fd2-4730-bd49-a9cd8299a065)
      )
      (fp_text user "GND" (at 2.54 1.27 unlocked) (layer "F.SilkS")
        (effects (font (size 0.8 0.8) (thickness 0.15)))
        (tstamp ca213826-0282-4b3a-840f-ec416dc34acf)
      )

      ${'' /* outline */}
      ${'' /* pins */}
      (pad "1" thru_hole circle (at 0 -1.27) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask) ${p.net.bat.str})
      (pad "2" thru_hole circle (at 0 1.27) (size 1.65 1.65) (drill 1.05) (layers *.Cu *.Mask) ${p.net.gnd.str})
    )`
}
