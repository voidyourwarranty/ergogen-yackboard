//
// This is the purely mechanical footprint for mounting an Adafruit Thumb Joystick 444.
// Note that wiring is done by hand and no connectors are included.
//
module.exports = {
    nets: {
    },
    params: {
        class: 'P', // for Pointer
        side: 'F'
    },
    body: p => `
    (module Ada444 (layer "F.Cu") (tedit 63717012)

      ${p.at /* parametric position */}
      ${'' /* footprint reference */}

      ${'' /* outline */}
      (fp_rect (start 9.25 5.85) (end 12.65 9.25) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_rect (start -10.95 7.55) (end -7.55 10.95) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_rect (start -9.25 -9.25) (end 9.25 9.25) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_rect (start -11.25 -5.08) (end -9.25 5.08) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_circle (center 0 0) (end 5.5 0) (layer "User.Drawings") (width 0.12) (fill none))

      ${'' /* pins */}
      (pad "" thru_hole circle (at 10.95 7.55) (size 2.8 2.8) (drill 2.2) (layers *.Cu *.Mask))
      (pad "" thru_hole circle (at -9.25 9.25) (size 2.8 2.8) (drill 2.2) (layers *.Cu *.Mask))
    )`
}
