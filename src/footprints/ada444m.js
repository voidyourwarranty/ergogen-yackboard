//
// This is the purely mechanical footprint for mounting an Adafruit Thumb Joystick 444, here the mirror image.
// Note that wiring is done by hand and no connectors are included.
//
module.exports = {
    nets: {
    },
    params: {
        class: 'P', // for Pointer
        side: 'F'
    },
    body: p => `
    (module Ada444m (layer "F.Cu") (tedit 63717012)

      ${p.at /* parametric position */}
      ${'' /* footprint reference */}

      ${'' /* outline */}
      (fp_rect (start -6.71 5.85) (end -10.11 9.25) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_rect (start 13.49 7.55) (end 10.09 10.95) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_rect (start 11.79 -9.25) (end -6.71 9.25) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_rect (start 13.79 -5.08) (end 11.79 5.08) (layer "User.Drawings") (width 0.12) (fill none))
      (fp_circle (center 2.54 0) (end -2.96 0) (layer "User.Drawings") (width 0.12) (fill none))

      ${'' /* pins */}
      (pad "" thru_hole circle (at -8.41 7.55) (size 2.8 2.8) (drill 2.2) (layers *.Cu *.Mask))
      (pad "" thru_hole circle (at 11.79 9.25) (size 2.8 2.8) (drill 2.2) (layers *.Cu *.Mask))
    )`
}
